package com.example.mycomp.organizer;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddNewLessonActivity extends AppCompatActivity {
    private ShowTimePickerDialog startTime;
    private ShowTimePickerDialog endTime;
    private int number;
    private int id;
    private String dayOfWeek;
    private boolean isUnderLine;
    @Override
    protected void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.add_new_lesson);

        number = getIntent().getExtras().getInt("Number");
        isUnderLine = getIntent().getBooleanExtra("UnderLine",false);
        dayOfWeek = getIntent().getStringExtra("DayOfWeek");
    }
    public void showTimePickerStart(View view){
        startTime = new ShowTimePickerDialog();
        startTime.show(getSupportFragmentManager(),"startTimePicker");
    }
    public void showTimePickerEnd(View view){
        endTime = new ShowTimePickerDialog();
        endTime.show(getSupportFragmentManager(),"endTimePicker");
    }
    public void addNewLesson(View view){
        EditText subject = findViewById(R.id.subject);
        EditText classNumber = findViewById(R.id.number_cabinet);
        EditText teacher = findViewById(R.id.teacher);
        EditText notes = findViewById(R.id.notes);

        int hourStart,hourEnd,minuteStart,minuteEnd;

        if(startTime!=null){
            hourStart = ((ShowTimePickerDialog)startTime).getHour_chosen();
            minuteStart = ((ShowTimePickerDialog)startTime).getMinute_chosen();
        }
        else{
            hourStart = 0;
            minuteStart = 0;
        }

        if(endTime!=null){
            hourEnd = ((ShowTimePickerDialog)endTime).getHour_chosen();
            minuteEnd = ((ShowTimePickerDialog)endTime).getMinute_chosen();
        }
        else{
            hourEnd = 0;
            minuteEnd = 0;
        }

        String startLesson = hourStart + ":" + minuteStart;
        String endLesson = hourEnd + ":" + minuteEnd;

            int underLine = (isUnderLine==true)?1:0;

        System.out.println("INT UNDERLINE: " + underLine);
            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app.db", MODE_PRIVATE, null);

            String query = "INSERT INTO NewLessons (numberOfWeek, subject, dayOfWeek, startTime, endTime, numberOfClass, teacher, notes, underLine) VALUES ('" + number + "','" + subject.getText().toString() + "','" + dayOfWeek + "','" + startLesson + "','" + endLesson + "','" + classNumber.getText().toString() + "','" + teacher.getText().toString() + "','" + notes.getText().toString() + "','" + underLine + "')";
        System.out.println(query);
            db.execSQL(query);
            db.close();

            Lesson lesson = new Lesson(number, subject.getText().toString(), dayOfWeek, startLesson, endLesson, classNumber.getText().toString(), teacher.getText().toString(), notes.getText().toString(),isUnderLine);

            System.out.println(lesson.toString());

        System.out.println("UNDER: " + isUnderLine);
              Data.get().getList().get(number).add(dayOfWeek,lesson,isUnderLine);

            finish();
        }
    }
