package com.example.mycomp.organizer;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

    public class  ShowTimePickerDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        private int hour_chosen;
        private int minute_chosen;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);


            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            setHour_chosen(hourOfDay);
            setMinute_chosen(minute);

            Toast.makeText(getContext(),"Time is chosen!",Toast.LENGTH_LONG).show();
        }

        public int getHour_chosen() {
            return hour_chosen;
        }
        public int getMinute_chosen() {
            return minute_chosen;
        }
        public void setHour_chosen(int hour_chosen) {
            this.hour_chosen = hour_chosen;
        }
        public void setMinute_chosen(int minute_chosen) {
            this.minute_chosen = minute_chosen;
        }
}
