package com.example.mycomp.organizer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class LessonsListFragment extends Fragment {
    private RecyclerView recyclerView;
    private LessonsAdapter adapter;
    private final int WEEKDAYS = Color.GREEN;
    private final int WEEKENDS = Color.RED;
    private final int WEEKDAYSUNDERLINE = Color.BLUE;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_list_weeks,container,false);

        recyclerView = view.findViewById(R.id.recycler_view_weeks);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);

        updateUI();
        return view;
    }
    private void updateUI(){
        ArrayList<Week> list = Data.get().getList();

        adapter = new LessonsAdapter(list);
        recyclerView.setAdapter(adapter);
    }
    private class LessonsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private LinearLayout layout_monday;
        private LinearLayout layout_tuesday;
        private LinearLayout layout_wednesday;
        private LinearLayout layout_thursday;
        private LinearLayout layout_friday;
        private LinearLayout layout_saturday;
        private LinearLayout layout_sunday;
        private TextView title_monday;
        private TextView title_tuesday;
        private TextView title_wednesday;
        private TextView title_thursday;
        private TextView title_friday;
        private TextView title_saturday;
        private TextView title_sunday;
        private TextView number_monday;
        private TextView number_tuesday;
        private TextView number_wednesday;
        private TextView number_thursday;
        private TextView number_friday;
        private TextView number_saturday;
        private TextView number_sunday;

        private LinearLayout layout_monday_under;
        private LinearLayout layout_tuesday_under;
        private LinearLayout layout_wednesday_under;
        private LinearLayout layout_thursday_under;
        private LinearLayout layout_friday_under;
        private LinearLayout layout_saturday_under;
        private LinearLayout layout_sunday_under;
        private TextView title_monday_under;
        private TextView title_tuesday_under;
        private TextView title_wednesday_under;
        private TextView title_thursday_under;
        private TextView title_friday_under;
        private TextView title_saturday_under;
        private TextView title_sunday_under;
        private TextView number_monday_under;
        private TextView number_tuesday_under;
        private TextView number_wednesday_under;
        private TextView number_thursday_under;
        private TextView number_friday_under;
        private TextView number_saturday_under;
        private TextView number_sunday_under;

        private TextView number_week;
        private Week week;
        private boolean isFill=false;

        public LessonsHolder(LayoutInflater inflater,ViewGroup group) {
            super(inflater.inflate(R.layout.lessons_list_item,group,false));

            number_week = itemView.findViewById(R.id.numberOfWeek);

            layout_monday = itemView.findViewById(R.id.layout_monday);
            layout_tuesday = itemView.findViewById(R.id.layout_tuesday);
            layout_wednesday = itemView.findViewById(R.id.layout_wednesday);
            layout_thursday = itemView.findViewById(R.id.layout_thursday);
            layout_friday = itemView.findViewById(R.id.layout_friday);
            layout_saturday = itemView.findViewById(R.id.layout_saturday);
            layout_sunday = itemView.findViewById(R.id.layout_sunday);

            number_monday = itemView.findViewById(R.id.number_monday);
            number_tuesday = itemView.findViewById(R.id.number_tuesday);
            number_wednesday = itemView.findViewById(R.id.number_wednesday);
            number_thursday = itemView.findViewById(R.id.number_thursday);
            number_friday = itemView.findViewById(R.id.number_friday);
            number_saturday = itemView.findViewById(R.id.number_saturday);
            number_sunday = itemView.findViewById(R.id.number_sunday);

            title_monday = itemView.findViewById(R.id.title_monday);
            title_tuesday = itemView.findViewById(R.id.title_tuesday);
            title_wednesday = itemView.findViewById(R.id.title_wednesday);
            title_thursday = itemView.findViewById(R.id.title_thursday);
            title_friday = itemView.findViewById(R.id.title_friday);
            title_saturday = itemView.findViewById(R.id.title_saturday);
            title_sunday = itemView.findViewById(R.id.title_sunday);

            number_monday_under = itemView.findViewById(R.id.number_monday_under);
            number_tuesday_under = itemView.findViewById(R.id.number_tuesday_under);
            number_wednesday_under = itemView.findViewById(R.id.number_wednesday_under);
            number_thursday_under = itemView.findViewById(R.id.number_thursday_under);
            number_friday_under = itemView.findViewById(R.id.number_friday_under);
            number_saturday_under = itemView.findViewById(R.id.number_saturday_under);
            number_sunday_under = itemView.findViewById(R.id.number_sunday_under);

            layout_monday_under = itemView.findViewById(R.id.layout_monday_under);
            layout_tuesday_under = itemView.findViewById(R.id.layout_tuesday_under);
            layout_wednesday_under = itemView.findViewById(R.id.layout_wednesday_under);
            layout_thursday_under = itemView.findViewById(R.id.layout_thursday_under);
            layout_friday_under = itemView.findViewById(R.id.layout_friday_under);
            layout_saturday_under = itemView.findViewById(R.id.layout_saturday_under);
            layout_sunday_under = itemView.findViewById(R.id.layout_sunday_under);

            title_monday_under = itemView.findViewById(R.id.title_monday_under);
            title_tuesday_under = itemView.findViewById(R.id.title_tuesday_under);
            title_wednesday_under = itemView.findViewById(R.id.title_wednesday_under);
            title_thursday_under = itemView.findViewById(R.id.title_thursday_under);
            title_friday_under = itemView.findViewById(R.id.title_friday_under);
            title_saturday_under = itemView.findViewById(R.id.title_saturday_under);
            title_sunday_under = itemView.findViewById(R.id.title_sunday_under);
        }

        public void bind(Week w){
            this.week = w;


                number_week.setText(String.valueOf(w.getNumberWeek()));

                if(w.getWeek().get("Monday")!=null && !w.getWeek().get("Monday").isUnderLine()) {
                    title_monday.setText(w.getWeek().get("Monday").getTitle());
                    number_monday.setText(w.getWeek().get("Monday").getNumberClass());
                    layout_monday.setBackgroundColor(WEEKDAYS);
                }
                if(w.getWeekUnder().get("Monday")!=null && w.getWeekUnder().get("Monday").isUnderLine()) {
                    title_monday_under.setText(w.getWeekUnder().get("Monday").getTitle());
                    number_monday_under.setText(w.getWeekUnder().get("Monday").getNumberClass());
                    layout_monday_under.setBackgroundColor(WEEKDAYSUNDERLINE);
                }
                 layout_monday.setOnClickListener(this);
                 layout_monday_under.setOnClickListener(this);

                if(w.getWeek().get("Tuesday")!=null && !w.getWeek().get("Tuesday").isUnderLine()) {
                    title_tuesday.setText(w.getWeek().get("Tuesday").getTitle());
                    number_tuesday.setText(w.getWeek().get("Tuesday").getNumberClass());
                    layout_tuesday.setBackgroundColor(WEEKDAYS);
                }
                if(w.getWeekUnder().get("Tuesday")!=null && w.getWeekUnder().get("Tuesday").isUnderLine()) {
                    title_tuesday_under.setText(w.getWeekUnder().get("Tuesday").getTitle());
                    number_tuesday_under.setText(w.getWeekUnder().get("Tuesday").getNumberClass());
                    layout_tuesday_under.setBackgroundColor(WEEKDAYSUNDERLINE);
                }
                layout_tuesday.setOnClickListener(this);
                layout_tuesday_under.setOnClickListener(this);

                if(w.getWeek().get("Wednesday")!=null && !w.getWeek().get("Wednesday").isUnderLine()) {
                    title_wednesday.setText(w.getWeek().get("Wednesday").getTitle());
                    number_wednesday.setText(w.getWeek().get("Wednesday").getNumberClass());
                    layout_wednesday.setBackgroundColor(WEEKDAYS);
                }
                if(w.getWeekUnder().get("Wednesday")!=null && w.getWeekUnder().get("Wednesday").isUnderLine()) {
                    title_wednesday_under.setText(w.getWeekUnder().get("Wednesday").getTitle());
                    number_wednesday_under.setText(w.getWeekUnder().get("Wednesday").getNumberClass());
                    layout_wednesday_under.setBackgroundColor(WEEKDAYSUNDERLINE);
                }
                layout_wednesday.setOnClickListener(this);
                layout_wednesday_under.setOnClickListener(this);

                if(w.getWeek().get("Thursday")!=null && !w.getWeek().get("Thursday").isUnderLine()) {
                    title_thursday.setText(w.getWeek().get("Thursday").getTitle());
                    number_thursday.setText(w.getWeek().get("Thursday").getNumberClass());
                    layout_thursday.setBackgroundColor(WEEKDAYS);
                }
                if(w.getWeekUnder().get("Thursday")!=null && w.getWeekUnder().get("Thursday").isUnderLine()) {
                    title_thursday_under.setText(w.getWeekUnder().get("Thursday").getTitle());
                    number_thursday_under.setText(w.getWeekUnder().get("Thursday").getNumberClass());
                    layout_thursday_under.setBackgroundColor(WEEKDAYSUNDERLINE);
                }
                layout_thursday.setOnClickListener(this);
                layout_thursday_under.setOnClickListener(this);

                if(w.getWeek().get("Friday")!=null && !w.getWeek().get("Friday").isUnderLine()) {
                    title_friday.setText(w.getWeek().get("Friday").getTitle());
                    number_friday.setText(w.getWeek().get("Friday").getNumberClass());
                    layout_friday.setBackgroundColor(WEEKDAYS);
                }
                if(w.getWeekUnder().get("Friday")!=null && w.getWeekUnder().get("Friday").isUnderLine()) {
                    title_friday_under.setText(w.getWeekUnder().get("Friday").getTitle());
                    number_friday_under.setText(w.getWeekUnder().get("Friday").getNumberClass());
                    layout_friday_under.setBackgroundColor(WEEKDAYSUNDERLINE);
                }
                layout_friday.setOnClickListener(this);
                layout_friday_under.setOnClickListener(this);

                if(w.getWeek().get("Saturday")!=null && !w.getWeek().get("Saturday").isUnderLine()) {
                    title_saturday.setText(w.getWeek().get("Saturday").getTitle());
                    number_saturday.setText(w.getWeek().get("Saturday").getNumberClass());
                    layout_saturday.setBackgroundColor(WEEKENDS);
                }
                if(w.getWeekUnder().get("Saturday")!=null && w.getWeekUnder().get("Saturday").isUnderLine()) {
                    title_saturday_under.setText(w.getWeekUnder().get("Saturday").getTitle());
                    number_saturday_under.setText(w.getWeekUnder().get("Saturday").getNumberClass());
                    layout_saturday_under.setBackgroundColor(WEEKENDS);
                }
                layout_saturday.setOnClickListener(this);
                layout_saturday_under.setOnClickListener(this);

                if(w.getWeek().get("Sunday")!=null && !w.getWeek().get("Sunday").isUnderLine()) {
                    title_sunday.setText(w.getWeek().get("Sunday").getTitle());
                    number_sunday.setText(w.getWeek().get("Sunday").getNumberClass());
                    layout_sunday.setBackgroundColor(WEEKENDS);
                }
                if(w.getWeekUnder().get("Sunday")!=null && w.getWeekUnder().get("Sunday").isUnderLine()) {
                    title_sunday_under.setText(w.getWeekUnder().get("Sunday").getTitle());
                    number_sunday_under.setText(w.getWeekUnder().get("Sunday").getNumberClass());
                    layout_sunday_under.setBackgroundColor(WEEKENDS);
                }
                layout_sunday.setOnClickListener(this);
                layout_sunday_under.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            String dayOfWeek=null;
            boolean underLine=false;

            switch (v.getId()){
                case R.id.layout_monday: dayOfWeek="Monday"; break;
                case R.id.layout_monday_under: dayOfWeek="Monday";underLine=true; break;
                case R.id.layout_tuesday: dayOfWeek="Tuesday"; break;
                case R.id.layout_tuesday_under: dayOfWeek="Tuesday"; underLine=true; break;
                case R.id.layout_wednesday: dayOfWeek="Wednesday"; break;
                case R.id.layout_wednesday_under: dayOfWeek="Wednesday"; underLine=true; break;
                case R.id.layout_thursday: dayOfWeek="Thursday"; break;
                case R.id.layout_thursday_under: dayOfWeek="Thursday"; underLine=true; break;
                case R.id.layout_friday: dayOfWeek="Friday"; break;
                case R.id.layout_friday_under: dayOfWeek="Friday"; underLine=true; break;
                case R.id.layout_saturday: dayOfWeek="Saturday"; break;
                case R.id.layout_saturday_under: dayOfWeek="Saturday"; underLine=true; break;
                case R.id.layout_sunday: dayOfWeek="Sunday"; break;
                case R.id.layout_sunday_under: dayOfWeek="Sunday"; underLine=true; break;
            }
            Bundle bundle = new Bundle();
            bundle.putInt("Number",week.getNumberWeek()-1);
            bundle.putBoolean("UnderLine",underLine);
            bundle.putString("DayOfWeek",dayOfWeek);

            System.out.println("N: " + week.getNumberWeek() + "\t" + underLine + "\t" + dayOfWeek);
            if(underLine)
                isFill = (week.getWeekUnder().get(dayOfWeek)==null)?false:true;
            else
                isFill = (week.getWeek().get(dayOfWeek)==null)?false:true;

            if(!isFill){
                Intent intent = new Intent(getContext(),AddNewLessonActivity.class);

                intent.putExtras(bundle);
                startActivity(intent);
            }
            else {
                EditDialogFragment fragment = new EditDialogFragment();
                fragment.setArguments(bundle);
                fragment.show(getFragmentManager(), "Edit dialog");
            }
        }
    }
    private class LessonsAdapter extends RecyclerView.Adapter<LessonsHolder>{
        private ArrayList<Week> weeks;
        public LessonsAdapter(ArrayList<Week> weeks){
            this.weeks = weeks;
        }
        @NonNull
        @Override
        public LessonsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new LessonsHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull LessonsHolder lessonsHolder, int i) {
            lessonsHolder.bind(weeks.get(i));
        }

        @Override
        public int getItemCount() {
            return weeks.size();
        }
    }
}
