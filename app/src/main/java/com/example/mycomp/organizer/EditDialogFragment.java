package com.example.mycomp.organizer;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import static android.content.Context.MODE_PRIVATE;

public class EditDialogFragment extends DialogFragment {
    String dayOfWeek=null;
    boolean isUnderLine = false;
    private Fragment dialogFragment;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();

        dialogFragment = this;


        int id = getArguments().getInt("Id");
        final int number = getArguments().getInt("Number");
        dayOfWeek = getArguments().getString("DayOfWeek");
        isUnderLine = getArguments().getBoolean("UnderLine");

        View view = inflater.inflate(R.layout.change_dialog,null);
        builder.setView(view)
                .setNegativeButton("Закрыть", null);

        TextView title = view.findViewById(R.id.dialog_title);
        final TextView day = view.findViewById(R.id.dialog_day_week);
        TextView startTime = view.findViewById(R.id.dialog_time);
        TextView classNumber = view.findViewById(R.id.dialog_number_class);

        Lesson l = isUnderLine==false?Data.get().getList().get(number).getWeek().get(dayOfWeek):Data.get().getList().get(number).getWeekUnder().get(dayOfWeek);

        title.setText(l.getTitle());
        day.setText(l.getDay());
        startTime.setText("С " + l.getStartLesson() + " до " + l.getEndLesson());
        classNumber.setText(l.getNumberClass());


        Button deleteButton = view.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isUnderLine==true)
                    Data.get().getList().get(number).getWeekUnder().put(dayOfWeek,null);
                else
                    Data.get().getList().get(number).getWeek().put(dayOfWeek,null);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment fragment = fm.findFragmentById(R.id.recycler_container);

                SQLiteDatabase db = getActivity().openOrCreateDatabase("app.db", MODE_PRIVATE, null);

                int underLine = (isUnderLine==true)?1:0;
                String query = "DELETE FROM NewLessons WHERE numberOfWeek = '" + number + "' AND dayOfWeek = '" + dayOfWeek + "' AND underLine = '" + underLine + "'";
                System.out.println(query);
                db.execSQL(query);
                db.close();

                if(fragment!=null)
                    fm.beginTransaction().remove(fragment).commit();

                Fragment fragmentNew = new LessonsListFragment();
                fm.beginTransaction()
                        .add(R.id.recycler_container,fragmentNew)
                        .commit();

                ((EditDialogFragment) dialogFragment).dismiss();
            }
        });
        Button editButton = view.findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),ChangeLessonActivity.class);
                intent.putExtra("Number",number);
                intent.putExtra("Underline",isUnderLine);
                intent.putExtra("DayOfWeek",dayOfWeek);

                ((EditDialogFragment) dialogFragment).dismiss();
                startActivity(intent);
            }
        });

        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
