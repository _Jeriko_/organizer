package com.example.mycomp.organizer;

public class Lesson {
    private int numberLesson;
    private String title;
    private String day;
    private String startLesson;
    private String endLesson;
    private String numberClass;
    private String nameTeacher;
    private String note;
    private boolean underLine;

    public Lesson(int numberLesson,String title, String day, String startLesson, String endLesson, String numberClass, String nameTeacher, String note, boolean underLine) {
        this.numberLesson = numberLesson;
        this.title = title;
        this.day = day;
        this.startLesson = startLesson;
        this.endLesson = endLesson;
        this.numberClass = numberClass;
        this.nameTeacher = nameTeacher;
        this.note = note;
        this.underLine = underLine;
    }
    public String toString(){
        return numberLesson + " " + title + " " + day + " " + startLesson + " " + endLesson + " " + numberClass + " " + nameTeacher + " " + note + " " + underLine;
    }

    public boolean isUnderLine() {
        return underLine;
    }

    public int getNumberLesson() {
        return numberLesson;
    }

    public String getTitle() {
        return title;
    }

    public String getDay() {
        return day;
    }

    public String getStartLesson() {
        return startLesson;
    }

    public String getEndLesson() {
        return endLesson;
    }

    public String getNumberClass() {
        return numberClass;
    }

    public String getNameTeacher() {
        return nameTeacher;
    }

    public String getNote() {
        return note;
    }
}
