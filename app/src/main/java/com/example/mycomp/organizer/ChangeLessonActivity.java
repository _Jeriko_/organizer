package com.example.mycomp.organizer;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ChangeLessonActivity extends AppCompatActivity {
    private ShowTimePickerDialog newTimeStart;
    private ShowTimePickerDialog newTimeEnd;
    private int week;
    private boolean isUnderLine;
    private String dayOfWeek;
    @Override
    protected void onCreate(Bundle savedInstanceBundle){
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.change_lesson);

        week = getIntent().getIntExtra("Number",1);
        isUnderLine = getIntent().getBooleanExtra("Underline",false);
        dayOfWeek = getIntent().getStringExtra("DayOfWeek");
    }
    public void showTimePickerStartChange(View view){
        newTimeStart = new ShowTimePickerDialog();
        newTimeStart.show(getSupportFragmentManager(),"New time start");
    }
    public void showTimePickerEndChange(View view){
        newTimeEnd = new ShowTimePickerDialog();
        newTimeEnd.show(getSupportFragmentManager(),"New time end");
    }
    public void changeLesson(View view){
        EditText subject = findViewById(R.id.title_change);
        EditText numberOfClass = findViewById(R.id.number_class_change);
        EditText teacher = findViewById(R.id.teacher_change);
        EditText notes = findViewById(R.id.notes_change);

        int hourStart,hourEnd,minuteStart,minuteEnd;

        if(newTimeStart!=null){
            hourStart = newTimeStart.getHour_chosen();
            minuteStart = newTimeStart.getMinute_chosen();
        }
        else{
            hourStart = 0;
            minuteStart = 0;
        }

        if(newTimeEnd!=null){
            hourEnd = newTimeEnd.getHour_chosen();
            minuteEnd = newTimeEnd.getMinute_chosen();
        }
        else{
            hourEnd = 0;
            minuteEnd = 0;
        }

        String startLesson = hourStart + ":" + minuteStart;
        String endLesson = hourEnd + ":" + minuteEnd;

            int underLine = (isUnderLine==true)?1:0;

            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app.db", MODE_PRIVATE, null);

            Lesson lesson = new Lesson(week,subject.getText().toString(), dayOfWeek, startLesson, endLesson, numberOfClass.getText().toString(), teacher.getText().toString(), notes.getText().toString(),isUnderLine);

            String query = "UPDATE NewLessons SET subject = '" + subject.getText().toString() + "' , startTime = '" + startLesson + "' , endTime = '" + endLesson + "' , numberOfClass = '" + numberOfClass.getText().toString() + "' , teacher = '" + teacher.getText().toString() + "' , notes = '" + notes.getText().toString() + "' WHERE numberOfWeek = '" + week + "' AND dayOfWeek = '" + dayOfWeek + "' AND underLine = '" + underLine + "'";
        System.out.println(query);

            db.execSQL(query);
            db.close();

            Data.get().getList().get(week).add(dayOfWeek,lesson,isUnderLine);

        finish();
    }
}
