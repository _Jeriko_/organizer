package com.example.mycomp.organizer;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    @Override
    protected void onStart(){
        super.onStart();


    }
    @Override
    protected void onResume(){
        super.onResume();

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("app.db",MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS NewLessons (numberOfWeek INTEGER,subject TEXT, dayOfWeek TEXT, startTime TEXT, endTime TEXT, numberOfClass TEXT, teacher TEXT, notes TEXT, underLine INTEGER)");

        Cursor query = db.rawQuery("SELECT * FROM NewLessons", null);

        if(query.moveToFirst()) {
            do {
                int number = query.getInt(0);
                String subject = query.getString(1);
                String dayWeek = query.getString(2);
                String start = query.getString(3);
                String end = query.getString(4);
                String numberClass = query.getString(5);
                String teacher = query.getString(6);
                String notes = query.getString(7);
                int underLine = query.getInt(8);

                boolean isUnderLine = underLine==0?false:true;

                Lesson lesson = new Lesson(number, subject, dayWeek, start, end, numberClass, teacher, notes,isUnderLine);
                Data.get().getList().get(number).add(dayWeek,lesson,isUnderLine);
            }
            while (query.moveToNext());
        }
        query.close();
        db.close();

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.recycler_container);

        if(fragment!=null)
            fm.beginTransaction().remove(fragment).commit();

        Fragment fragmentNew = new LessonsListFragment();
        fm.beginTransaction()
                .add(R.id.recycler_container,fragmentNew)
                .commit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
