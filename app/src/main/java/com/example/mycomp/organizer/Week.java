package com.example.mycomp.organizer;

import java.util.HashMap;
import java.util.Map;

public class Week {
    private int numberWeek;
    private Map<String,Lesson> week;
    private Map<String,Lesson> weekUnder;

    public Week(int numberWeek){
        this.numberWeek = numberWeek;
        week = new HashMap<>();
        week.put("Monday",null);
        week.put("Tuesday",null);
        week.put("Wednesday",null);
        week.put("Thursday",null);
        week.put("Friday",null);
        week.put("Saturday",null);
        week.put("Sunday",null);

        weekUnder = new HashMap<>();
        weekUnder.put("Monday",null);
        weekUnder.put("Tuesday",null);
        weekUnder.put("Wednesday",null);
        weekUnder.put("Thursday",null);
        weekUnder.put("Friday",null);
        weekUnder.put("Saturday",null);
        weekUnder.put("Sunday",null);
    }
    public void add(String day,Lesson lesson,boolean under){
        if(!under)
            week.put(day,lesson);
        else
            weekUnder.put(day,lesson);
    }
    public Map<String,Lesson> getWeek(){
        return week;
    }
    public Map<String,Lesson> getWeekUnder(){
        return weekUnder;
    }
    public int getNumberWeek(){ return numberWeek; }
}
