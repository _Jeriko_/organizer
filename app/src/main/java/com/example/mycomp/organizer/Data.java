package com.example.mycomp.organizer;

import java.util.ArrayList;

public class Data {
    private ArrayList<Week>list;
    private static Data dl;

    private Data(){
        list = new ArrayList<>();
        for(int i=0;i<6;i++)
            list.add(new Week(i+1));
    }
    public static Data get(){
        if(dl==null)
            dl = new Data();
        return dl;
    }
    public ArrayList<Week> getList(){
        return list;
    }
}
